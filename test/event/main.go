/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"github.com/k0kubun/pp"
	"gitlab.com/skyhuborg/tracker/internal/event"
	"time"
)

func main() {
	event := event.NewEvent()

	event.Start()

	//pp.Println("Initialized Event object:")
	//pp.Println(event)

	pp.Printf("Event %s started at %s\n", event.GetId(), event.StartTime)
	time.Sleep(time.Millisecond * 2529)

	//pp.Println("In Progress Event Object:")
	//pp.Println(event)
	time.Sleep(time.Millisecond * 1129)

	event.Stop()

	pp.Printf("Event %s stopped at %s\n", event.GetId(), event.EndTime)
	//pp.Printf("Event '%s' lasted for %s ms\n", event.GetId(), event.GetDuration())
	pp.Printf("Event %s started at %s ended at %s and lasted for %s ms\n", event.GetId(), event.StartTime, event.EndTime, event.GetDuration())
	//pp.Println(event)
}
