/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"flag"
	"fmt"
	"gitlab.com/skyhuborg/tracker/internal/video"
	//"gocv.io/x/gocv"
)

type Environment struct {
	DeviceUri     string
	VideoPath     string
	ThumbnailPath string
	Duration      int
}

var env Environment

func parseArgs() {
	deviceUri := flag.String("device", "", "URI to video string")
	videoPath := flag.String("video-path", "video.mp4", "File to write video stream")
	thumbnailPath := flag.String("thumbnail-path", "thumbnail.png", "File to write video thumbnail")
	duration := flag.Int("duration", 10, "Duration of capture in second")
	flag.Parse()

	env.DeviceUri = *deviceUri
	env.VideoPath = *videoPath
	env.ThumbnailPath = *thumbnailPath
	env.Duration = *duration
}

func main() {
	parseArgs()
	var v video.Video

	err := v.Open(env.DeviceUri)

	if err != nil {
		fmt.Printf("Failed opening device: %s\n", err)
		return
	}
	defer v.Close()

	motionDetector := video.NewMotionDetector()
	motionDetector.SetMinimumArea(3000)
	motionDetector.EnableBoxing(true)

	v.RegisterVideoOutput(motionDetector)

	v.Start()
}
