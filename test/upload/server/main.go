package main

import (
	"gitlab.com/skyhuborg/tracker/internal/upload"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var (
		server *upload.Server
		config upload.ServerConfig
	)
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func(server *upload.Server) {
		<-c
		log.Println("Shutting down server")
		server.Close()
		os.Exit(0)
	}(server)

	config.ListenPort = 8100
	config.ChunkSize = 4096

	server = upload.NewServer(&config)

	server.Start()
}
