package main

import (
	"gitlab.com/skyhuborg/tracker/internal/upload"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var (
		config upload.ClientConfig
	)

	config.ServerAddr = "localhost:8100"
	config.ChunkSize = 1024
	RunTest(config)

	config.ServerAddr = "localhost:8100"
	config.ChunkSize = 4096
	RunTest(config)

	config.ServerAddr = "localhost:8100"
	config.ChunkSize = 8192
	RunTest(config)
	return
}

func RunTest(config upload.ClientConfig) {
	var (
		err    error
		client upload.Client
	)

	log.Printf("Test ChunkSize=%d\n",
		config.ChunkSize)

	client = upload.NewClient(&config)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func(client upload.Client) {
		<-c
		log.Println("Shutting down client")
		client.Close()
		os.Exit(0)
	}(client)

	err = client.Connect()

	if err != nil {
		log.Printf("Start() failed: %s\n", err)
		return
	}

	client.Upload("test.dat")
	client.Upload("test1.dat")
	client.Upload("test2.dat")
	client.Upload("test3.dat")
	client.Upload("test4.dat")
	client.Upload("test.dat")
	client.Upload("test1.dat")
	client.Upload("test2.dat")
	client.Upload("test3.dat")
	client.Upload("test4.dat")

	client.Close()

}
