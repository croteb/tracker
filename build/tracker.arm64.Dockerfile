FROM ubuntu:focal

ENV PACKAGES="ffmpeg git make sqlite3 gcc x264 wget ca-certificates libopencv-dev pkg-config zip g++ zlib1g-dev unzip python python3-numpy tar"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes ${PACKAGES}

RUN wget https://dl.google.com/go/go1.14.2.linux-arm64.tar.gz && \
    tar -C /usr/local -xzf go1.14.2.linux-arm64.tar.gz

ENV GOPATH=/go/
ENV GO111MODULE=on
ENV PATH=${PATH}:/usr/local/go/bin

RUN mkdir /build
COPY go.mod /build/go.mod
COPY go.sum /build/go.sum
RUN cd /build && go mod download
ADD . /build/
RUN cd /build && \
    mkdir -p /skyhub/db && \
    mkdir -p /skyhub/etc && \
    scripts/create_trackerdb.sh && \
    cp tracker.db /skyhub/db/tracker.db && \
    make tracker && \
    cp /build/cmd/bin/arm64/linux/tracker /skyhub/tracker && \
    ldd /skyhub/tracker
WORKDIR /skyhub
CMD ["/skyhub/tracker"]


# FROM ubuntu

# ENV PACKAGES="ffmpeg opencv git build-base make \
#         tiff-dev sqlite gcc x264" \
# export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin/
# ENV GOPATH="$HOME/go/"
# ENV GO111MODULE=on

# RUN apt-get update && apt-get install --no-install-recommends --yes ${PACKAGES}


# RUN mkdir -p /build && \
#     wget https://dl.google.com/go/go1.13.6.linux-arm64.tar.gz && \
#     sudo tar -C /usr/local -xzf go1.13.6.linux-arm64.tar.gz

# COPY go.mod /build/go.mod
# COPY go.sum /build/go.sum
# RUN cd /build && go mod download
# ADD . /build/
# RUN cd /build && \
#     mkdir -p /skyhub/db && \
#     mkdir -p /skyhub/etc && \
#     scripts/create_trackerdb.sh && \
#     cp tracker.db /skyhub/db/tracker.db && \
#     make tracker && \
#     cp /build/cmd/bin/arm64/linux/tracker /skyhub/tracker && \
#     ldd /skyhub/tracker
# WORKDIR /skyhub
# CMD ["/skyhub/tracker"]

# FROM registry.gitlab.com/skyhub/gocv-alpine:4.2.0-buildstage as build-stage
# FROM golang:1.14.1-alpine3.11

# COPY --from=build-stage /usr/local/lib64 /usr/local/lib64
# COPY --from=build-stage /usr/local/lib64/pkgconfig/opencv4.pc /usr/local/lib64/pkgconfig/opencv4.pc
# COPY --from=build-stage /usr/local/include/opencv4/opencv2 /usr/local/include/opencv4/opencv2

#     LD_LIBRARY_PATH=/usr/local/lib64 \
#     PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig          

#TPU

# wget https://github.com/google-coral/edgetpu/archive/diploria2.tar.gz
# tar xvfz diploria2.tar.gz
# cd edgetpu-diploria2

# # install udev rules
# install libedgetpu/edgetpu-accelerator.rules /etc/udev/rules.d/99-edgetpu-accelerator.rules

# # reload rules without reboot
# # udevadm control --reload-rules && udevadm trigger

# # install header files
# install libedgetpu/edgetpu.h /usr/local/include/edgetpu.h
# install libedgetpu/edgetpu_c.h /usr/local/include/edgetpu_c.h

# # install libraries (direct mode)
# install libedgetpu/direct/k8/libedgetpu.so.1 /usr/local/lib/libedgetpu.so.1

# # install libraries (direct mode), arm64
# #install libedgetpu/direct/aarch64/libedgetpu.so.1 /usr/local/lib/libedgetpu.so.1

# # create symlink
# ln -s /usr/local/lib/libedgetpu.so.1 /usr/local/lib/libedgetpu.so.1.0

# Tensorflow lite


# apt install git pkg-config zip g++ zlib1g-dev unzip python python3-numpy
# pip install future numpy
# wget -c https://github.com/bazelbuild/bazel/releases/download/0.27.1/bazel_0.27.1-linux-x86_64.deb
# apt install ./bazel_0.27.1-linux-x86_64.deb
# git clone https://github.com/tensorflow/tensorflow.git
# cd tensorflow
# git checkout r2.1
# yes '' | TF_NEED_GDR=0 TF_NEED_AWS=0 TF_NEED_GCP=0 TF_NEED_CUDA=0 TF_NEED_HDFS=0 TF_NEED_OPENCL_SYCL=0 TF_NEED_VERBS=0 TF_NEED_MPI=0 TF_NEED_MKL=0 TF_NEED_JEMALLOC=1 TF_ENABLE_XLA=0 TF_NEED_S3=0 TF_NEED_KAFKA=0 TF_NEED_IGNITE=0 TF_NEED_ROCM=0 | ./configure
# bazel build --config monolithic //tensorflow/lite:libtensorflowlite.so
# bazel build --config opt --config monolithic //tensorflow/lite/experimental/c:libtensorflowlite_c.so
# cd ..
