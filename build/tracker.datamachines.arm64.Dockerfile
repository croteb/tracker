FROM datamachines/jetsonnano-cuda_tensorflow_opencv:10.0_2.1.0_3.4.10-20200423
# FROM datamachines/jetsonnano-cuda_tensorflow_opencv:10.0_2.1.0_4.3.0-20200423
# FROM datamachines/cudnn_tensorflow_opencv:10.2_2.1.0_4.3.0-20200423

ENV PACKAGES="ffmpeg git make sqlite3 gcc x264 wget ca-certificates libopencv-dev pkg-config zip g++ zlib1g-dev unzip python python3-numpy tar"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes ${PACKAGES}

RUN wget https://dl.google.com/go/go1.14.2.linux-arm64.tar.gz && \
    tar -C /usr/local -xzf go1.14.2.linux-arm64.tar.gz

ENV GOPATH=/go/
ENV GO111MODULE=on
ENV PATH=${PATH}:/usr/local/go/bin

RUN mkdir /build
COPY go.mod /build/go.mod
COPY go.sum /build/go.sum
RUN cd /build && go mod download
ADD . /build/
RUN cd /build && \
    mkdir -p /skyhub/db && \
    mkdir -p /skyhub/etc && \
    scripts/create_trackerdb.sh && \
    cp tracker.db /skyhub/db/tracker.db && \
    make tracker && \
    cp /build/cmd/bin/arm64/linux/tracker /skyhub/tracker && \
    ldd /skyhub/tracker
WORKDIR /skyhub
CMD ["/skyhub/tracker"]

