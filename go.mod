module gitlab.com/skyhuborg/tracker

go 1.13

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20200428022330-06a60b6afbbc // indirect
	github.com/enriquebris/goconcurrentqueue v0.6.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/golang/protobuf v1.4.1
	github.com/google/uuid v1.1.1
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.5.1 // indirect
	github.com/matipan/computer-vision v0.0.0-20190726221744-604c2c3f10f1
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6
	github.com/pkg/errors v0.9.1
	github.com/stratoberry/go-gpsd v0.0.0-20161204231141-54ddcfa61f47
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/skyhuborg/proto-tracker-controller-go v0.0.0-20200510134017-345ffdcaa695
	gitlab.com/skyhuborg/proto-tracker-ui-go v0.0.0-20200428194214-c1a137072311
	gitlab.com/skyhuborg/proto-trackerd-go v0.0.0-20200428194549-fcc698dac7a9
	gitlab.com/skyhuborg/trackerdb v0.0.0-20200428194408-64b3b4517d8a
	go.bug.st/serial v1.1.0
	gocv.io/x/gocv v0.23.0
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79 // indirect
	golang.org/x/net v0.0.0-20200506145744-7e3656a0809f // indirect
	golang.org/x/sys v0.0.0-20200509044756-6aff5f38e54f // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20200507105951-43844f6eee31 // indirect
	google.golang.org/grpc v1.29.1
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.2.8
)
