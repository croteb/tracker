/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package tracker

import (
	"gocv.io/x/gocv"
	"log"
	"time"
)

func CaptureVideo(device string, uri string, thumbnail string, duration int) {
	var (
		maxOpenAttempts int = 10
		nOpenAttempts   int
	)

retry_open:
	cam, err := gocv.OpenVideoCapture(device)

	if err != nil {

		if nOpenAttempts >= maxOpenAttempts {
			log.Printf("Error opening camera, maxAttempts=%d reached: %v\n", nOpenAttempts, device)
			return
		}

		nOpenAttempts++
		time.Sleep(5 * time.Second)
		goto retry_open
	}

	defer cam.Close()

	img := gocv.NewMat()
	defer img.Close()

	if ok := cam.Read(&img); !ok {
		log.Printf("Cannot read camera %v\n", uri)
		return
	}

	//writer, err := gocv.VideoWriterFile(uri, "avc1", 30, img.Cols(), img.Rows(), true)
	writer, err := gocv.VideoWriterFile(uri, "avc1", 30, img.Cols(), img.Rows(), true)

	if err != nil {
		log.Printf("Error opening output file: %v\n", uri)
		return
	}
	defer writer.Close()

	for i := 0; i < duration*30; i++ {
		if ok := cam.Read(&img); !ok {
			log.Printf("Camera closed: %v\n", uri)
			return
		}

		if i == 0 {
			ok := gocv.IMWrite(thumbnail, img)

			if ok == false {
				log.Printf("Error writing thumbnail")
			}
		}

		if img.Empty() {
			continue
		}

		writer.Write(img)
	}
}
