/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package video

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
)

type VideoStream struct {
	/* video metadata */
	framePerSecond float64
	frameHeight    int
	frameWidth     int
}

type Video struct {
	Device      string
	Username    string
	Password    string
	Handle      *gocv.VideoCapture
	State       *event.State
	VideoOutput []VideoProcessor
	stream      VideoStream
}

type VideoProcessor interface {
	Name() string
	Init(stream *VideoStream)
	SetState(state *event.State)
	GetChannel() chan *gocv.Mat
	Process()
	Close()
}

func (v *Video) calculateFps() float64 {
	img := gocv.NewMat()
	nFrames := 120
	dumpFrames := 60
	i := 0

	for i < dumpFrames {
		v.Handle.Read(&img)
		i++
	}

	start := time.Now()

	i = 0
	for i < nFrames {
		v.Handle.Read(&img)
		i++
	}

	end := time.Now()
	elapsed := end.Sub(start)
	return float64(nFrames) / elapsed.Seconds()
}

func (v *Video) Open(device string) (err error) {
	var (
		maxOpenAttempts int = 10
		nOpenAttempts   int
	)

	v.Device = device

retry_open:
	log.Printf("Opening camera: %v\n", v.Device)

	v.Handle, err = gocv.OpenVideoCapture(v.Device)

	if err != nil {
		log.Printf("Error opening camera: %v\n", v.Device)

		if nOpenAttempts >= maxOpenAttempts {
			log.Printf("Error opening camera, maxAttempts=%d reached: %v\n", nOpenAttempts, device)
			return
		}

		nOpenAttempts++
		time.Sleep(5 * time.Second)
		goto retry_open
	}

	v.stream.framePerSecond = v.Handle.Get(gocv.VideoCaptureFPS)
	v.stream.frameHeight = int(v.Handle.Get(gocv.VideoCaptureFrameHeight))
	v.stream.frameWidth = int(v.Handle.Get(gocv.VideoCaptureFrameWidth))

	if v.stream.framePerSecond >= 120 {
		log.Printf("Invalid framerate (%f), sampling video stream\n", v.stream.framePerSecond)
		v.stream.framePerSecond = v.calculateFps()
		log.Printf("Detected %f frame rate\n", v.stream.framePerSecond)
	}

	log.Printf("Video: Opening video stream %s %dx%d@%ffps\n",
		v.Device,
		v.stream.frameWidth,
		v.stream.frameHeight,
		v.stream.framePerSecond)

	return nil
}

func (v *Video) Close() {
	v.Handle.Close()
}

func (v *Video) SetState(state *event.State) {
	v.State = state
}

func (v *Video) RegisterVideoOutput(proc VideoProcessor) {
	v.VideoOutput = append(v.VideoOutput, proc)
}

func (v *Video) Start() {
	/* start output threads */
	var (
		err    error
		output string
	)

	for _, processor := range v.VideoOutput {
		output += fmt.Sprintf("%s ", processor.Name())
		processor.Init(&v.stream)

		if v.State != nil {
			log.Printf("Video: Attaching %s to EventState\n", processor.Name())
		}

		go processor.Process()
	}
	log.Printf("Video: Starting with the following Processors: [ %s]\n", output)

	img := gocv.NewMat()
	defer img.Close()

	for {
		ok := v.Handle.Read(&img)

		if !ok {
			log.Printf("Video: Read failed. Re-opening device %v\n", v.Device)
		retry_reopen:
			err = v.Open(v.Device)

			if err != nil {
				log.Printf("Video: Re-opening device %v failed wi th: %s\n", v.Device, err)
				time.Sleep(1 * time.Second)
				goto retry_reopen
			}
			log.Printf("Video: Read failed %v\n", v.Device)
			continue
		}

		if img.Empty() {
			continue
		}

		if img.Cols() == 0 || img.Rows() == 0 {
			continue
		}

		dims := img.Size()

		if dims[0] <= 2 || dims[1] <= 2 {
			continue
		}

		for _, processor := range v.VideoOutput {
			ch := processor.GetChannel()
			ch <- &img
		}
	}
}
