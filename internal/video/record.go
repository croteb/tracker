/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package video

import (
	"fmt"
	"github.com/enriquebris/goconcurrentqueue"
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
	"log"
)

type Record struct {
	Channel  chan *gocv.Mat
	Stream   *VideoStream
	DataPath string
	state    *event.State
	/* video recording state */
	videoUri     string
	thumbnailUri string
	videoWriter  *gocv.VideoWriter
	channels     []chan VideoRecording

	frameQueue  goconcurrentqueue.Queue
	isRecording bool
}

type VideoRecording struct {
	EventId   string
	Uri       string
	Thumbnail string
}

func NewRecord() *Record {
	p := Record{}
	p.isRecording = false
	return &p
}

func (v *Record) Name() string {
	return "Record"
}

func (v *Record) SetState(state *event.State) {
	v.state = state
	v.state.Ref()
}

func (v *Record) SetDataPath(dataPath string) {
	v.DataPath = dataPath
}

func (v *Record) Init(stream *VideoStream) {
	v.frameQueue = goconcurrentqueue.NewFIFO()
	v.Channel = make(chan *gocv.Mat)
	v.Stream = stream

	go v.processFrame()
}

func (v *Record) GetChannel() chan *gocv.Mat {
	return v.Channel
}

func (v *Record) Close() {
	return
}

func (v *Record) Start(ev *event.Event) {
	var err error
	v.isRecording = true

	v.videoUri = fmt.Sprintf("%s/video/%s.mp4", v.DataPath, ev.GetId())

	v.videoWriter, err = gocv.VideoWriterFile(
		v.videoUri,
		//"hvc1",
		"avc1",
		float64(v.Stream.framePerSecond),
		v.Stream.frameWidth,
		v.Stream.frameHeight,
		true)

	if err != nil {
		log.Printf("Video: Failed to create writer: %s\n", err)
	}
}

func (v *Record) generateThumbnail(ev *event.Event, img *gocv.Mat) (success bool) {
	v.thumbnailUri = fmt.Sprintf("%s/thumbnail/%s.png", v.DataPath, ev.GetId())

	success = gocv.IMWrite(v.thumbnailUri, *img)

	if success == false {
		log.Printf("Failed writing thumbnail for event '%s'\n", ev.GetId())
	}

	return
}

func (v *Record) Stop(ev *event.Event) {
	var (
		videoRecording VideoRecording
	)
	v.isRecording = false

	var finalFrames []*gocv.Mat

	for {

		data, err := v.frameQueue.Dequeue()
		if err != nil {
			break
		}

		frame, ok := data.(*gocv.Mat)

		if ok {
			finalFrames = append(finalFrames, frame)
		}
	}

	for _,img := range finalFrames {
		v.videoWriter.Write(*img)
	}

	v.videoWriter.Close()

	// notify callers of new video recording
	videoRecording.EventId = ev.GetId()
	videoRecording.Uri = v.videoUri
	videoRecording.Thumbnail = v.thumbnailUri

	for _, ch := range v.channels {
		ch <- videoRecording
	}

	v.videoWriter = nil

	v.videoUri = ""
	v.thumbnailUri = ""
}

func (v *Record) Register(ch chan VideoRecording) {
	v.channels = append(v.channels, ch)
}

func (v *Record) GetIsRecording() bool {
	return v.isRecording
}

func (v *Record) Process() {
	for {
		img := <-v.Channel
		v.frameQueue.Enqueue(img)
	}
}

func (v *Record) processFrame() {
	var (
		ev   *event.Event
		item interface{}
		err  error
		img  *gocv.Mat
		ok   bool
	)

	for {
		ev = v.state.GetEvent()

		if ev.GetInProgress() {
			if !v.GetIsRecording() {
				v.Start(ev)
				go v.generateThumbnail(ev, img)
			}

			err := v.videoWriter.Write(*img)

			if err != nil {
				log.Printf("Video: Recording write error on event '%s': %s\n", ev.GetId(), err)
			}
		} else {
			if v.GetIsRecording() {
				v.Stop(ev)
				ev.Done()
			}
		}

		item, err = v.frameQueue.DequeueOrWaitForNextElement()

		if err != nil {
			log.Printf("Dequeue error: %s\n", err)
			continue
		}

		img, ok = item.(*gocv.Mat)

		if !ok {
			log.Printf("Image assertion failed\n")
			continue
		}

	}
}
