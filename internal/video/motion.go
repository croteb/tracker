/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package video

import (
	"gitlab.com/skyhuborg/tracker/internal/event"
	"gocv.io/x/gocv"
	"image"
	"image/color"
	"log"
	"time"

	"github.com/enriquebris/goconcurrentqueue"
)

type MotionDetector struct {
	MinimumArea  float64
	DrawBoxes    bool
	DrawContours bool
	ShowWindow   bool
	Channel      chan *gocv.Mat
	state        *event.State
	Stream       *VideoStream
	frameQueue   goconcurrentqueue.Queue
}

func NewMotionDetector() *MotionDetector {
	p := MotionDetector{}
	p.MinimumArea = 3000
	p.DrawContours = false
	p.DrawBoxes = false
	p.ShowWindow = false

	return &p
}

func (v *MotionDetector) Name() string {
	return "MotionDetector"
}

func (v *MotionDetector) SetState(state *event.State) {
	v.state = state
}

func (v *MotionDetector) SetMinimumArea(minimumArea float64) {
	v.MinimumArea = minimumArea
}

func (v *MotionDetector) EnableContours(enableCountours bool) {
	v.DrawContours = enableCountours
}

func (v *MotionDetector) EnableBoxing(enableBoxing bool) {
	v.DrawBoxes = enableBoxing
}

func (v *MotionDetector) EnableWindow(enableWindow bool) {
	v.ShowWindow = enableWindow
}

func (v *MotionDetector) Init(stream *VideoStream) {
	v.frameQueue = goconcurrentqueue.NewFIFO()
	v.Channel = make(chan *gocv.Mat)
	v.Stream = stream

	go v.processFrame()
}

func (v *MotionDetector) GetChannel() chan *gocv.Mat {
	return v.Channel
}

func (v *MotionDetector) Close() {
	return
}

func (v *MotionDetector) Process() {
	for {
		img := <-v.Channel
		v.frameQueue.Enqueue(img)
	}
}

func (v *MotionDetector) processFrame() {
	var (
		window          *gocv.Window
		src             event.Source
		lastTriggerSent time.Time
	)

	e := v.state.GetEvent()

	src = event.Source{}
	src.Set("MotionDetector", event.SourceVideo)

	if v.ShowWindow == true {
		window = gocv.NewWindow("Motion Window")
		defer window.Close()
	}

	imgDelta := gocv.NewMat()
	defer imgDelta.Close()

	imgThresh := gocv.NewMat()
	defer imgThresh.Close()

	mog2 := gocv.NewBackgroundSubtractorMOG2()
	defer mog2.Close()

	for {
		item, err := v.frameQueue.DequeueOrWaitForNextElement()

		if err != nil {
			log.Printf("Dequeue error: %s\n", err)
			continue
		}

		img, ok := item.(*gocv.Mat)

		if !ok {
			log.Printf("Image assertion failed\n")
			continue
		}

		mog2.Apply(*img, &imgDelta)

		gocv.Threshold(imgDelta, &imgThresh, 25, 255, gocv.ThresholdBinary)

		kernel := gocv.GetStructuringElement(gocv.MorphRect, image.Pt(3, 3))
		defer kernel.Close()

		dims := kernel.Size()

		step := kernel.Step()

		if kernel.Empty() || dims[0] <= 2 || dims[1] <= 2 || step < 1 {
			continue
		}
		if imgThresh.Empty() || dims[0] <= 2 || dims[1] <= 2 || step < 1 {
			continue
		}

		gocv.Dilate(imgThresh, &imgThresh, kernel)

		contours := gocv.FindContours(imgThresh, gocv.RetrievalExternal, gocv.ChainApproxSimple)
		for _, c := range contours {
			area := gocv.ContourArea(c)
			if area < v.MinimumArea {
				continue
			}

			if e.GetInProgress() == false {
				if e.GetIsComplete() == true {
					e.SetSource(&src)
					v.state.Trigger(&src)
					lastTriggerSent = time.Now()
				}
			} else {
				elapsed := time.Since(lastTriggerSent).Seconds()

				if elapsed >= 1 {
					v.state.Trigger(&src)
					lastTriggerSent = time.Now()
				}
			}

			if v.DrawBoxes == true {
				rect := gocv.BoundingRect(c)
				gocv.Rectangle(img, rect, color.RGBA{0, 0, 255, 0}, 2)
			}
		}

		if v.ShowWindow == true {
			if !img.Empty() {
				window.IMShow(*img)
				if window.WaitKey(1) == 27 {
					break
				}
			}
		}
	}
}
