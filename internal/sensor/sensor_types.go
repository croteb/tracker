/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	"fmt"
	"github.com/golang/protobuf/ptypes"
	pb "gitlab.com/skyhuborg/proto-trackerd-go"
	"time"
)

type SensorType string

const (
	GpsTPVType       = "GpsTpv"
	GpsVType         = "GpsVType"
	TimeType         = "Time"
	PositionType     = "Position"
	TemperatureType  = "Temperature"
	AltitudeType     = "Altitude"
	AirPressureType  = "AirPressure"
	GyroscopeType    = "Gyroscope"
	AccelerationType = "Acceleration"
	AngleType        = "Angle"
	MagnetometerType = "Magnetometer"
	LonLatType       = "LonLat"
)

type GPS_TPVReport struct {
	Class  string    `json:"class"`
	Tag    string    `json:"tag"`
	Device string    `json:"device"`
	Mode   int32     `json:"mode"`
	Time   time.Time `json:"time"`
	Ept    float64   `json:"ept"`
	Lat    float64   `json:"lat"`
	Lon    float64   `json:"lon"`
	Alt    float64   `json:"alt"`
	Epx    float64   `json:"epx"`
	Epy    float64   `json:"epy"`
	Epv    float64   `json:"epv"`
	Track  float64   `json:"track"`
	Speed  float64   `json:"speed"`
	Climb  float64   `json:"climb"`
	Epd    float64   `json:"epd"`
	Eps    float64   `json:"eps"`
	Epc    float64   `json:"epc"`
}

func (x GPS_TPVReport) String() string {
	return fmt.Sprintf("class=%s tag=%s device=%s mode=%x time=%d Ept=%f Lat=%f Lon=%f Alt=%f Epx=%f Epy=%f Epv=%f Track=%f Speed=%f Climb=%f Epd=%f Eps=%f Epc=%f",
		x.Class,
		x.Tag,
		x.Device,
		x.Mode,
		x.Time,
		x.Ept,
		x.Lat,
		x.Lon,
		x.Alt,
		x.Epx,
		x.Epy,
		x.Epv,
		x.Track,
		x.Speed,
		x.Climb,
		x.Epd,
		x.Eps,
		x.Epc)
}

func (x GPS_TPVReport) Pack() *pb.GPS_TPVReport {
	ts, _ := ptypes.TimestampProto(x.Time)
	o := &pb.GPS_TPVReport{
		Class:  x.Class,
		Tag:    x.Tag,
		Device: x.Device,
		Mode:   int32(x.Mode),
		Time:   ts,
		Ept:    x.Ept,
		Lat:    x.Lat,
		Lon:    x.Lon,
		Alt:    x.Alt,
		Epx:    x.Epx,
		Epy:    x.Epy,
		Epv:    x.Epv,
		Track:  x.Track,
		Speed:  x.Speed,
		Climb:  x.Climb,
		Epd:    x.Epd,
		Eps:    x.Eps,
		Epc:    x.Epc,
	}
	return o
}

type Time struct {
	Year        byte
	Month       byte
	Day         byte
	Hour        byte
	Minute      byte
	Second      byte
	Millisecond int16
}

func (x Time) String() string {
	return fmt.Sprintf("20%d-%d-%d %d:%d:%.3f",
		x.Year,
		x.Month,
		x.Day,
		x.Hour,
		x.Minute,
		float32(x.Second)+float32(x.Millisecond/1000))
}

func (x Time) Pack() *pb.Time {
	o := &pb.Time{
		Year:        int32(x.Year),
		Month:       int32(x.Month),
		Day:         int32(x.Day),
		Hour:        int32(x.Hour),
		Minute:      int32(x.Minute),
		Second:      int32(x.Second),
		Millisecond: int32(x.Millisecond),
	}
	return o
}

type Acceleration struct {
	A [3]int16
	T int16
}

func (x Acceleration) String() string {
	return fmt.Sprintf("%.3f %.3f %.3f",
		float32(x.A[0])/32768*16,
		float32(x.A[1])/32768*16,
		float32(x.A[2])/32768*16)
}

func (x Acceleration) Pack() *pb.Acceleration {
	xa := make([]float32, 3)

	xa[0] = float32(x.A[0]) / 32768 * 16
	xa[1] = float32(x.A[1]) / 32768 * 16
	xa[2] = float32(x.A[2]) / 32768 * 16

	o := &pb.Acceleration{
		A: xa,
	}
	return o
}

type Gyroscope struct {
	W [3]int16
	T int16
}

func (x Gyroscope) String() string {
	return fmt.Sprintf("%.3f %.3f %.3f",
		float32(x.W[0])/32768*2000,
		float32(x.W[1])/32768*2000,
		float32(x.W[2])/32768*2000)
}

func (x Gyroscope) Pack() *pb.Gyroscope {
	xw := make([]int32, 3)

	xw[0] = int32(x.W[0])
	xw[1] = int32(x.W[1])
	xw[2] = int32(x.W[2])

	o := &pb.Gyroscope{
		W: xw,
	}
	return o
}

type Angle struct {
	Angle [3]int16
	T     int16
}

func (x Angle) String() string {
	return fmt.Sprintf("%.3f %.3f %.3f",
		float32(x.Angle[0])/32768*180,
		float32(x.Angle[1])/32768*180,
		float32(x.Angle[2])/32768*180)
}

func (x Angle) Pack() *pb.Angle {
	xangle := make([]int32, 3)

	xangle[0] = int32(x.Angle[0]) / 32768 * 180
	xangle[1] = int32(x.Angle[1]) / 32768 * 180
	xangle[2] = int32(x.Angle[2]) / 32768 * 180

	o := &pb.Angle{
		Angle: xangle,
	}

	return o
}

type Magnetometer struct {
	H [3]int16
	T int16
}

func (x Magnetometer) String() string {
	return fmt.Sprintf("%d %d %d",
		x.H[0],
		x.H[1],
		x.H[2])
}

func (x Magnetometer) Pack() *pb.Magnetometer {
	xh := make([]int32, 3)

	xh[0] = int32(x.H[0])
	xh[1] = int32(x.H[1])
	xh[2] = int32(x.H[2])

	o := &pb.Magnetometer{
		H: xh,
	}

	return o
}

type AirPressure struct {
	Pressure int32
	Altitude int32
}

func (x AirPressure) String() string {
	return fmt.Sprintf("Pressure: %d Height: %.2f",
		x.Pressure,
		float32(x.Altitude)/100)
}

func (x AirPressure) Pack() *pb.AirPressure {
	o := &pb.AirPressure{
		Pressure: x.Pressure,
		Altitude: x.Altitude / 100}

	return o
}

type LonLat struct {
	Lon int
	Lat int
}

func (x LonLat) String() string {
	return fmt.Sprintf("Longitude: %ld Deg: %.5fm Lattitude: %ld Deg: %.5fm",
		x.Lon/10000000,
		float64(x.Lon%10000000)/1e5,
		x.Lat/10000000,
		float64(x.Lat%10000000)/1e5)
}

func (x LonLat) Pack() *pb.LonLat {
	o := &pb.LonLat{
		Lon: float64(x.Lon%10000000) / 1e5,
		Lat: float64(x.Lat%10000000) / 1e5,
	}

	return o
}

/* this was in the witmotion docs,
   witmotion requires an attached gps,
   we're not using this */
type GPSV struct {
	GPSHeight   int16
	GPSYaw      int16
	GPSVelocity int16
}

func (x GPSV) String() string {
	return fmt.Sprintf("GPSHeight: %.1fm GPSYaw: %.1fDeg GPSV: %.3fkm/h\n",
		float32(x.GPSHeight)/10,
		float32(x.GPSYaw)/10,
		float32(x.GPSVelocity)/1000)
}

func (x GPSV) Pack() *pb.GPSV {
	o := &pb.GPSV{
		GPSHeight:   float32(x.GPSHeight) / 10,
		GPSYaw:      float32(x.GPSYaw) / 10,
		GPSVelocity: float32(x.GPSVelocity) / 1000,
	}

	return o
}
