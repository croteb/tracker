/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package sensor

import (
	"github.com/orcaman/concurrent-map"
	"github.com/stratoberry/go-gpsd"
	"log"
)

type GPSDSensor struct {
	Addr    string
	data    cmap.ConcurrentMap
	session *gpsd.Session
}

func (s *GPSDSensor) name() string {
	return "GPSD"
}

func (s *GPSDSensor) read() map[string]interface{} {
	return s.data.Items()
}

func (s *GPSDSensor) tpvFilter(r interface{}) {
	var tpv GPS_TPVReport
	report := r.(*gpsd.TPVReport)
	tpv.Class = report.Class
	tpv.Tag = report.Tag
	tpv.Mode = int32(report.Mode)
	tpv.Time = report.Time
	tpv.Ept = report.Ept
	tpv.Lat = report.Lat
	tpv.Lon = report.Lon
	tpv.Alt = report.Alt
	tpv.Epx = report.Epx
	tpv.Epy = report.Epy
	tpv.Epv = report.Epv
	tpv.Track = report.Track
	tpv.Speed = report.Speed
	tpv.Climb = report.Climb
	tpv.Epd = report.Epd
	tpv.Eps = report.Eps
	tpv.Epc = report.Epc
	s.data.Set(GpsTPVType, tpv)
}

func gpsgo(s *GPSDSensor) {
	done := s.session.Watch()
	<-done
}

func (s *GPSDSensor) init() bool {
	var (
		err error
	)

	s.data = cmap.New()

	s.session, err = gpsd.Dial(s.Addr)

	if err != nil {
		log.Printf("GPS failed: %s\n", err)
		return false
	}

	s.session.AddFilter("TPV", s.tpvFilter)

	go gpsgo(s)

	return true
}

func (s *GPSDSensor) close() bool {
	return true
}
